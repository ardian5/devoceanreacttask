import React from 'react';
import './App.css';
import { Route, Routes } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import { Single } from './Pages/Single';
import { Home } from './Pages/Home';

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <React.Suspense fallback={"Loading..."}>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/:ID" element={<Single />} />
          </Routes>
      </React.Suspense>
    </QueryClientProvider>
  );
}

export default App;
