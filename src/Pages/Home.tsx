import { useQuery } from "react-query";

import { Header } from "../components/Header/Header";
import { fetchData } from "../api/ReactQuery";
import { Business } from "../api/types";
import { ListItem } from "../components/ListItem/ListItem";
import { GeneralTexts } from "../enum/enums";

export const Home = () => {
  const { data: business } = useQuery(["business"], fetchData);

  return (
    <div className="min-h-screen bg-ghost-white">
      <Header />
      <div className="container mx-auto relative pt-32 h-screen overflow-hidden">
        <div className="flex bg-white mb-1">
          <p className="w-1/3 px-10 py-4 font-bold text-pink-800">{GeneralTexts.NAME}</p>
          <p className="px-10 py-4 font-bold text-pink-800">{GeneralTexts.DESCRIPTION}</p>
        </div>
        <div className="h-[calc(100vh-190px)] overflow-y-auto">
          {business?.map(({ name, description, id }: Business) => (
            <ListItem key={id} name={name} description={description} id={id} />
          ))}
        </div>
      </div>
    </div>
  );
};
  