import { useEffect } from "react";
import { useState } from 'react';
import { useQuery } from "react-query";
import { useParams } from 'react-router-dom';

import { Header } from "../components/Header/Header";
import { fetchData } from "../api/ReactQuery";
import { Business } from "../api/types";
import { GeneralTexts } from "../enum/enums";

export const Single = (): JSX.Element => {
  const [singleBusindesData, setSingleBusinessData] = useState<Business>();
  const [nearbyPlaces, setNearbyPlaces] = useState<Business[]>();
  const { data: business } = useQuery('business', fetchData);
  const { ID } = useParams();

  useEffect(() => {
    if(business && ID) {
      const businessData: Business | undefined = business.find(({ id }: Business) => id === ID);
      if(businessData) {
        setSingleBusinessData(businessData);
        if (businessData.address) {
          const getNearbyPlaces = business.filter((business) => (business?.address?.city === businessData?.address?.city) && (business?.id !== ID));
          setNearbyPlaces(getNearbyPlaces);
        }
      }
    }
  }, [business, ID]);
  

  return (
    <div className="min-h-screen bg-ghost-white">
      <Header />
      <div className="container mx-auto pt-20">
        <img className="w-full mt-20 h-[560px] object-cover" src={singleBusindesData?.image} alt={singleBusindesData?.name} />
        <div className="flex p-10">
          <div className="w-1/2 flex">
            <div className="w-1/2">
              <h2 className="py-8 text-2xl">{GeneralTexts.ADDRESS}</h2>
              <p>
                {singleBusindesData?.address?.number}
                <span className="px-1">{singleBusindesData?.address?.city}</span>
                {singleBusindesData?.address?.street}
              </p>
              <p>
                <span className="pr-1">{singleBusindesData?.address?.country},</span>
                {singleBusindesData?.address?.zip}
              </p>
            </div>
            <div className="w-1/2">
              <h2 className="py-8 text-2xl">{GeneralTexts.CONTACT}</h2>
              <p>
                {singleBusindesData?.phone}
              </p>
              <p>
                {singleBusindesData?.email}
              </p>
            </div>
          </div>
          <div className="w-1/2 bg-white px-6 py-8">
            <h2 className="pb-8 text-2xl">{GeneralTexts.NEARBY_PLACES}</h2>
            {nearbyPlaces?.map((business: Business) => (
              <div className="flex bg-ghost-white p-3 mb-1" key={business?.id}>
                <p className="w-1/4">{business.name}</p>
                <span className="px-1">{business?.address?.street}</span>
                <span className="px-1">{business?.address?.country},</span>
                <span className="px-1">{business?.address?.city}</span>
                <span className="px-1">{business?.address?.number}</span>
                <span className="px-1">{business?.address?.zip}</span>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
