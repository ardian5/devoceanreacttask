import axios from "axios";
import { Business } from "./types";

const API = process.env.REACT_APP_API || "";

export const fetchData = async () => {
  const response = new Promise<Business[]>((resolve) => {
    axios.get(API).then((res) => resolve(res.data))
  });
  return response;
};
