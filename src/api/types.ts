export type Address = {
  city: string,
  country: string,
  number: string,
  street:string,
  zip: string,
};

export interface Business {
  address: Address,
  description: string,
  email:  string,
  id:  string,
  image:  string,
  name: string,
  phone: string,
};