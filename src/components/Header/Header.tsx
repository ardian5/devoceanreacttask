import { useNavigate } from "react-router-dom";

const logo = require("../../assets/images/logo.png");

export const Header = (): JSX.Element => {
  const navigate = useNavigate();
  
  return (
    <div className="fixed w-full z-10 top-0 flex items-center justify-center p-6 shadow-md bg-white">
        <img src={logo} alt="Business" onClick={() => navigate('/')} role="button" />
    </div>
)};
