import { useNavigate } from 'react-router-dom';
import { ListItemProps } from "./types";

export const ListItem = ({ name, description, id }: ListItemProps) => {
  const navigate = useNavigate();
  return (
  <div className="flex mb-1 bg-white hover:bg-ghost-white-hover cursor-pointer" onClick={() => navigate(`/${id}`)}>
    <p className="w-1/3 px-10 py-4">{name}</p>
    <p className="px-10 py-4">{description}</p>
  </div>
)};
