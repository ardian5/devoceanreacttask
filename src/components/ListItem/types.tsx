export type ListItemProps = {
  id: string;
  name: string;
  description: string;
};