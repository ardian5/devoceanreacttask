export enum GeneralTexts {
    NAME = "NAME",
    DESCRIPTION = "DESCRIPTION",
    ADDRESS = "Address",
    CONTACT = "Contact",
    NEARBY_PLACES = "Nearby Places",
};
