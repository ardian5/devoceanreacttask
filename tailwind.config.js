/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'ghost-white': '#f8f8fa',
        'ghost-white-hover': '#f4f4f5',
      },
      spacing: {
        '21': '84px',
      }
    },
  },
  plugins: [],
}
